class Producto < ApplicationRecord
	validates :codigo, uniqueness: true
	validates :codigo, :descripcion, :precio, presence: true
	has_attached_file :imagen, styles: { medium: "300x300", thumb: "100x100" }, default_url: "/images/no-existe.png"
  	validates_attachment_content_type :imagen, content_type: /\Aimage\/.*\z/

  	has_attached_file :pdf, default_url: "/images/no-existe.png", path: ":rails_root/pdf/:id/:filename",   url: ":rails_root/pdf/:id/:filename"

    validates_attachment_content_type :pdf, content_type: ['application/pdf']            	

    def url_pdf
    	 Rails.root.join("pdf/",self.id.to_s,self.pdf_file_name||="nofile.pdf").to_s#agregarmos la ruta url con el nombre dle archivo
    end
	
end
