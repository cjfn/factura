Rails.application.routes.draw do
  resources :users
  get 'page/index'

  resources :productos do
  member do
    get :muestra_pdf
  end
end
  
  
  root to: 'visitors#index'

end
